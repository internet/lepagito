# =============================
# 1. standard pelican variables
# =============================
SITEURL      = 'http://lepagito.artliba.org'
SITENAME     = 'Lepagito'
AUTHOR       = 'I. Lepage and P. Hesse'
SITESUBTITLE = 'A lovely pelican theme'
TIMEZONE     = 'Europe/Paris'
DEFAULT_LANG = 'en'
LOCALE       = 'C'

PATH         = 'content'
STATIC_PATHS = []

OUTPUT_PATH  = 'output/'
#DELETE_OUTPUT_DIRECTORY = True # before site generation

DISPLAY_CATEGORIES_ON_MENU = False # choose a custom top menu here
MENUITEMS = (
  ('All',        'index.html'),
  ('Quickstart', 'quickstart.html'),
  ('Goodies',    'goodies.html'),
  ('Options',    'options.html'),
  ('#Tags',      'tags.html'),
)
DEFAULT_PAGINATION =   6; # article summaries per page
SUMMARY_MAX_LENGTH = 100; # counted in words

RELATIVE_URLS                    = True
SLUGIFY_SOURCE                   = 'basename' 
ARTICLE_URL   = ARTICLE_SAVE_AS  = '{slug}.html'
CATEGORY_URL  = CATEGORY_SAVE_AS = '{slug}.html'
TAG_URL       = TAG_SAVE_AS      = '{slug}.html'
AUTHOR_URL    = AUTHOR_SAVE_AS   = '{slug}.html'
ARCHIVES_SAVE_AS                 = 'archives.html'
YEAR_ARCHIVE_SAVE_AS             = 'archive_{date:%Y}.html'
MONTH_ARCHIVE_SAVE_AS            = 'archive_{date:%Y}_{date:%m}.html'
# =============================
# 2. Lepagito theme
# =============================
THEME = '/home/hesse/dvt-perso/lepagito'
FOOTERMENUITEMS = (
  ('About',    'about.html'),
  ('License',  'license.html'),
)
TAGS_BY_GROUPS = (
  ('Pelican', ('tag-group', 'navigation', 'plugin', 'pelicanconf.py')),
)
#FOOTERMENUFEED    = False; # default is True
#ARCHIVES_CALENDAR = False; # default is True
LEPAGITO_NAVIGATION_SWAP = True
LEPAGITO_COLOR = { 'cold': 0, 'file': 'color.css' } # the default value
# internationalization (i18n): here are the default values
I18N_CATEGORIES  = 'Categories'
I18N_CATEGORY    = 'Category'
I18N_TAGS        = '#Tags'
I18N_TOP         = 'Top'
I18N_PAGE        = 'Page'
I18N_READ_MORE   = 'Read more'
I18N_ARCHIVES    = 'Archives'
I18N_FEED_ALL_ATOM = 'Feed'
I18N_NEWEST      = 'Newest'
I18N_OLDEST      = 'Oldest'
I18N_FIRST       = 'First'
I18N_LAST        = 'Last'
I18N_BACK_TO     = 'Back to'
I18N_OTHERS      = 'Others'
I18N_PREV        = 'Prev'
I18N_NEXT        = 'Next'
# =============================
# 3. plugins, optionals
# =============================
PLUGIN_PATHS  = ['/home/hesse/dvt-perso/galleria/pelican/plugins', 
                 '/home/hesse/dvt-perso/theme-config/pelican/plugins']
#                '/home/hesse/pelican-plugins/theme-config/pelican/plugins']
PLUGINS = ['search', 'sitemap', 'galleria', 'neighbors', 'theme_config']
# ------------------
# 3.1. search plugin
# ------------------
SEARCH_MODE = 'output'
# -------------------
# 3.2. sitemap plugin
# -------------------
SITEMAP = {
    'format': 'xml',
    'priorities': {
        'articles': 1.0,
        'pages':    0.5,
        'indexes':  0.5
    },
    'changefreqs': {
        'articles': 'weekly',
        'pages':    'monthly',
        'indexes':  'monthly'
    }
}
# --------------------
# 3.3. galleria plugin
# --------------------
##THEME_TEMPLATES_OVERRIDES      = ['/home/hesse/dvt-perso/galleria/template']
GALLERIA_LIBRARY_PATH          = ['/home/hesse/dvt-perso/lepagito/doc/content']
LEPAGITO_GALLERY_CSS_FILE      = 'galleria.css'
# for lepagito templates:
GALLERIA_PROCESS_DIR           = 'galleria'
GALLERIA_RESIZE_JOBS           = 10
GALLERIA_VERBOSE               = True
GALLERIA_PROCESS = {
    'image-icon': {
        'type': 'image',
        'ops': ['50x50', '+profile "*"'],
    },
    'image-large': {
        'type': 'image',
        'ops': ['1200x900', '+profile "*"'],
    },
    # https://developer.mozilla.org/en-US/docs/Learn/HTML/Multimedia_and_embedding/Responsive_images
    'responsive-small': {
        'type': 'responsive-image',
        'sizes': (
            '(min-width: 768px) 650px, '
            '(min-width: 1200px) 800px, '
            '100vw'
        ),
        'srcset': [
            ('600w', ['600x450', '+profile "*"']),
            ('800w', ['800x600', '+profile "*"']),
        ],
        'default': '800w',
    },
    # https://developer.mozilla.org/en-US/docs/Web/HTML/Element/picture
   'picture-tag': {
       'type': 'picture',
       'sources': [
           {
               'name': 'default',
               'media': '(min-height: 640px)',
               'srcset': [
                   ( '640w', [ '640x480',  '+profile "*"']),
                   ('1024w', ['1024x683',  '+profile "*"']),
                   ('1600w', ['1600x1200', '+profile "*"']),
               ],
               'sizes': '100vw',
           },
           {
               'name': 'source-1',
               'srcset': [
                   ('1x', ['600x450']),
                   ('2x', ['800x600']),
               ]
           },
       ],
       'default': ('default', '640w'),
   },
}
GALLERIA_DEFAULT_CLASS         = 'responsive-small'
GALLERIA_SINGLE_CLASS          = 'image-large'
GALLERIA_ICON_CLASS            = 'image-icon'
GALLERIA_DEFAULT_MAX_HEIGHT    = '600px'
