.. include:: common.txt

About
=====
:date: 2023-02-14
:status: hidden

`Lafilacroche`_
deigned and developed the initial CSS file used by this `pelican`_ theme:
it defines its main design, fonts and colors.
`Pirogh`_,
contributed by adding some extensions,
e.g. calendar, search, groups of tags, and internationalization.

Contact
-------
You want to:

* write me a few words
* ask for something
* inform me of a trouble on this blog, a typos
  or an incomprehensible explanation in one of my post
* or anything else?

You can email me at:

.. class:: center

`pirogh.hesse@artliba.org`_

