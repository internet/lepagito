.. include:: common.txt

Custom colors
-------------
:date: 2021-01-14
:category: Options
:tags: color, pelicanconf.py

All colors of the `lepagito`_ theme are customizable.
The default color file is ``color.css`` and an alternate
file could be specicied by using the
``LEPAGITO_COLOR_CSS_FILE`` variable
in the ``pelicanconf.py``, such as::

  LEPAGITO_COLOR_CSS_FILE  = 'mycustomcolor.css'

Modifying by hand the ``color.css`` file is not obvious:
for convenience, a python script ``mk_custom_color_css.py`` is provided
in the ``static/css`` directory.
It admits three arguments: the two first are the rotation angles
for the hue of the warm and cold colors, and the third argument is
the optional basename for the css output file. 
For zero rotation, enter the command::

  python mk_custom_color_css.py 0 0 mycustomcolor

.. _palette: palette.artliba.org

It will regenerate exactly the default color theme
but in the file ``mycustomcolor.css``. It also provides
``mycustomcolor.pig``, which is the color set,
suitable for the `palette`_ visualization software.
This default color theme corresponds to a palette composed to two groups
of colors, the warm magenta and cold blue groups:

.. galleria::
    mycustomcolor-0-0-theme.jpg
    mycustomcolor-0-0-palette.jpg

.. _CIECAM Jab: https://en.wikipedia.org/wiki/CIECAM02

The second image represent the color palette in the 
`CIECAM Jab`_ coordinate system, in the (a,b) plane: observe the
two groups of colors.
The present graph is obtained thanks to the `palette`_ visualization software.

Now, let us rotate the cold blue group, until reaching the
yellow position on the palette::
  
  python mk_custom_color_css.py 0 195 mycustomcolor
  cd ../../doc
  pelican content
  firefox output/index.html &

We then obtain:

.. galleria::
    mycustomcolor-0-195-theme.jpg
    mycustomcolor-0-195-palette.jpg

Let us now rotate the same group until it reaches the magenta position::

  python mk_custom_color_css.py 0 120 mycustomcolor

The two groups are now superposed and we obtain a camaïeu:

.. galleria::
    mycustomcolor-0-120-theme.jpg
    mycustomcolor-0-120-palette.jpg

Finally, the first warm group is also rotated until it reaches the blue position::

  python mk_custom_color_css.py 240 120 mycustomcolor

The two initial groups are now swapped:

.. galleria::
    mycustomcolor-240-120-theme.jpg
    mycustomcolor-240-120-palette.jpg


