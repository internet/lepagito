.. include:: common.txt

Archives and calendar
---------------------
:date: 2022-04-14
:category: Goodies
:tags: calendar, pelicanconf.py

The `Archives`_, available at the footer menu,
starts with a calendar, that provides a concise view.
Years and months are both links to periodic archives.
In the ``pelicanconf.py`` file, the two following variables
should be defined, for instance as::

  YEAR_ARCHIVE_SAVE_AS  = 'archive_{date:%Y}.html'
  MONTH_ARCHIVE_SAVE_AS = 'archive_{date:%Y}_{date:%m}.html'

The calendar view do not refer to any pelican plugin:
this is an original functionality of the `Lepagito`_ theme.
If you don't want the calendar to appear on the `archives`_ page,
you could either not define at least one of these variable,
or set::

  ARCHIVES_CALENDAR = False

By default, this variable is True.
