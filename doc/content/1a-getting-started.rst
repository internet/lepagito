.. include:: common.txt

Getting started
---------------
:date: 2023-02-14
:category: Quickstart
:tags: pelicanconf.py, install, navigation

`Lepagito`_ is a lovely pelican theme.

Observe it first in action on the `lepagito`_ web site.
Also, it is used by both the `Pirogh`_  author's blog,
the `Greuze-Cottave`_ blog for painters and designers
and the `galleria`_ pelican plugin documentation.

You're interested? Fascinated? Let's start!
First, download and install the theme::

  git clone https://gricad-gitlab.univ-grenoble-alpes.fr/internet/lepagito

For navigation and searches, this theme includes the optional
`neighbors`_, `search`_  and `sitemap`_ pelican plugins::

  sudo apt install pelican python3-pip python3.11-venv python3-full python-is-python3
  python -m venv ~/mypy
  source ~/mypy/bin/activate
  pip install pelican-neighbors
  pip install pelican-search
  pip install pelican-sitemap
  pip install pelican-theme-config
  
The first ``apt`` command is specific to `GNU/Linux Debian`_ systems
and should be adapted to your environment.
The `neighbors`_ optional plugin allows a convenient navigation between articles.
The `search`_ plugin provides a self-hosted search capability to your site.
Optionally, if you wish to customize the theme colors::

  pip install colorspacious Pillow colormath

Also, the optional `galleria`_ plugin, for photo galleries::

  git clone https://gricad-gitlab.univ-grenoble-alpes.fr/internet/galleria

The `search`_  pelican plugin depends itself upon `stork`_, 
that should also be installed.
For instance, for `GNU/Linux Debian`_ systems, just enter::

  wget https://files.stork-search.net/releases/v1.6.0/stork-ubuntu-20-04
  chmod a+x stork-ubuntu-20-04
  sudo mv -i stork-ubuntu-20-04 /usr/local/bin/stork

If you are in trouble when installing `stork`_ on your computer,
don't worry: you could just edit the ``pelicanconf.py`` file
and remove both this optional plugin
together with the ``SEARCH_MODE`` variable::

  PLUGINS = ['neighbors', 'sitemap', 'galleria']
  #SEARCH_MODE = 'output'

The `sitemap`_ optional plugin generates a ``sitemap.xml`` file
to facilitate the indexing of your site by search engines.
Finally, the `galleria`_ optional plugin facilitates the
management of image galleries.

The documentation of this theme is itself a pelican blog using the present theme,
so the source files of the documentation present
a practical example on how to use 
pelican together with this theme.
You could now check your computer environment:
produce and read the documentation::

  cd doc
  pelican content
  firefox output/index.html &

Next, if you already have a pelican blog,
you just have to edit your *pelicanconf.py* file an change your theme as::

  THEME = '/my-full-path-to/lepagito'

Finally, just enter as usual::

  pelican content
  firefox output/index.html &
