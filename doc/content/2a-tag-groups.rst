.. include:: common.txt

Groups of tags
--------------
:date: 2022-05-14
:category: Goodies
:tags: tag-group, pelicanconf.py

In many cases, the tag list becomes very long
and contains heterogeneous tags.
It should be better to group them: this feature is 
implemented in the `Lepagito`_ theme.
For instance, in the ``pelicanconf.py`` file::

  TAGS_BY_GROUPS = (
    ('Color',  ('blue', 'red', 'yellow')),
    ('State',  ('solid', 'liquid', 'gas')),
    ('Firtname', ('Isabelle', 'Pierre', 'Paul', 'Jacques')),)

If a tag appears in an article but has any defined group,
it will appear in the 'Others' group.
