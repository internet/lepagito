.. include:: common.txt

Sitemap
-------
:date: 2021-03-14
:category: Options
:tags: sitemap, plugin, pelicanconf.py

The `sitemap`_ plugin
generates a ``sitemap.xml`` file
to facilitate the indexing of your site by search engines.
There is nothing to configure, it works as a black-box.
