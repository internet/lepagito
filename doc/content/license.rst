.. include:: common.txt

License
=======
:date: 2023-02-14
:status: hidden

.. _Creative Commons BY NC SA 4.0: https://creativecommons.org/licenses/by-nc-sa/4.0/

Unless otherwise stated and with the exception of my logo, the contents of this blog are published under the
`Creative Commons BY NC SA 4.0`_ license.

`Creative Commons BY NC SA 4.0`_
--------------------------------

This license authorizes you to:

*  **Share** — copy and redistribute the material in any medium or format 

*  **Adapt** — remix, transform, and build upon the material

Under the following terms:

* **Attribution** — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

* **NonCommercial** — You may not use the material for commercial purposes.

* **ShareAlike** — If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original. 

**No additional restrictions** — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.


Exceptions
----------
Some content may be published under different licenses that may be more permissive or restrictive.
This is specified whenever this is the case.

The logo
--------
The logo, which roughly represents a heart, is subject to copyright and intellectual property.
It is not royalty-free, you cannot use, copy, reproduce or modify it without my prior permission.
Note: this logo evolves from time to time, copyright and intellectual property applies to all its different versions.

The stork files
---------------
The search function is implemented thanks to the `search`_ pelican plugin,
which uses `stork`_ to generate a search index.
This function is implemented as a self-hosted site search capability to your site.
For that, the `Lepagito`_ theme redistribute the following files::

	static/css/stork.css
	static/css/stork-dark.css
	static/js/stork.js
	static/js/stork.js.map
	static/js/stork.wasm

These files are distributed under the 
`Apache license version 2.0`_.

The watercolor photographies
----------------------------
The watercolor photographies are provided for a `galleria`_
plugin demo when used together with the `lepagito`_ theme.
These files are copyrighted by `Pirogh`_ under the 
`Creative Commons BY NC ND 4.0`_ licence.
