.. include:: common.txt

Self-hosted search
------------------
:date: 2021-04-14
:category: Options
:tags: search, plugin, pelicanconf.py

The search function appears at the right of the top menu bar.
Rather than give up control and privacy to third-party search engine corporations,
this function adds elegant and self-hosted search capability to your site.
Last but not least, searches are **really fast**.
The search function is implemented thanks to the pelican `search`_ plugin.
The search engine is activated by the following line
in the ``pelicanconf.py`` file::

  SEARCH_MODE = 'output'

Note that the search function requires that the blog is installed:
it do not work in the off-line mode.
If you do not need searches on your site, e.g. your tags are sufficients,
just remove the previous line from the ``pelicanconf.py`` 
and the search function will disappear from the top menu bar.

If you are in trouble when installing `stork`_, that is necessary
for running the pelican `search`_ plugin, you could just remove
both this plugin and previous line from the ``pelicanconf.py``::

  PLUGINS = ['neighbors']
  #SEARCH_MODE = 'output'
