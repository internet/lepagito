.. include:: common.txt

The file pelicanconf.py
-----------------------
:date: 2023-01-14
:category: Quickstart
:tags: pelicanconf.py, install, navigation

Here is a complete example of the ``pelicanconf.py`` file,
as used for the present documentation of the theme.
It starts by setting some standard `pelican`_ variables, as usual.
Then, come the theme's and the optional plugins.

.. include:: ../pelicanconf.py
  :literal:
  :tab-width: 8

Remark in the first 
that all URLs are chosen as relative here::

  RELATIVE_URLS = True
  ARTICLE_URL   = ARTICLE_SAVE_AS  = '{slug}.html'
  CATEGORY_URL  = CATEGORY_SAVE_AS = '{slug}.html'

Also, all output files (articles, categories, tags, ...)
are written in the same "output/" directory, without any hierarchy.
This setting is convenient, since it allows
to develop and fully test the blog off-line
before to upload it on its final destination:
relative URLs, such as in the ``MENUITEMS``, 
work both in off-line and in-line modes.

The second part of the configuration file
contains the specific settings of the `Lepagito`_ theme:
these settings are be described in the sections
of the `Goodies`_ category.
Finally, the third part
of the ``pelicanconf.py`` file
sets the optional plugins:
see the `Options`_ category.
