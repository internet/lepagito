.. include:: common.txt

Photo galleries
---------------
:date: 2021-02-14
:category: Options
:tags: galleria, plugin, pelicanconf.py
:image: 2022-09-watercolor-grece-03-fresque-tirynthe.jpg

Last but not the least, the `galleria`_ plugin allows to easily manage galleries of images.
Especially, it nicely manages the automatic generation of thumbnails from images,
as shown in the following gallery of 
watercolors due to `Pirogh`_:

.. galleria::
    2022-09-watercolor-grece-03-fresque-tirynthe.jpg
    2022-09-watercolor-grece-06-fresque-akrotiri-hirondelles.jpg
    2022-09-watercolor-grece-07-fresque-akrotiri-boxeurs.jpg
    :max-height: 350px

When you click on a thumbnail of a picture, a full resolution version
of this picture is displayed.
Also, try to resize the browser window and
observe the responsive display of the gallery:
the gallery is automatically readjusted and this is especially useful
when displaying on mobile phones.
Finally the thumbnail's image resolution itself is also responsive,
in the sense that it takes into account the physical resolution
of our window and screen resolution.
See the source file of this documentation for a complete example
and also the documentation of the `galleria`_ plugin for more.

To associate an image with an article, i.e. as an icon, add the metadata field.
For restructured text ``.rst`` files, it writes::

  :image: myimage.jpg

to the header of an article.
Associated images improve navigation:
icons are displayed together with article titles
when browsing pages and `Archives`_.

