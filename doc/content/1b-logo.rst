.. include:: common.txt

The logo
--------
:date: 2023-02-13
:category: Quickstart
:tags: pelicanconf.py, logo

The logo at the top of each page is sill the heat,
which is specific to the `Lepagito`_ theme.
Your blog should have its own logo, since
the  `Lepagito`_ logo is copyrighted (see `License`_).
To do that, copy first logos from 
`Lepagito`_ to your environment, exactly as::

  mkdir content/theme
  cp -a ~/pelican-themes/lepagito/static/img theme

In the *content/theme/img* directory, the files are::

	logo.png
	favicon-16x16.png
	favicon-32x32.png
	favicon.ico
	apple-touch-icon.png
	mstile-150x150.png
	site.webmanifest

	entry_foooter.png
	browserconfig.xml

The first six files are different implementations of the same
logo and should be modified.
The seventh file should also be modified.
The eigth file is the blue pencil article footer:
it could be changed or left as is, as you prefer.
The last file could be left as is.

Finally, edit again your ``pelicanconf.py`` file as::

    STATIC_PATHS = ['images', 'theme']

and run again pelican: your logo has been changed.
