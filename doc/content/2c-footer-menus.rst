.. include:: common.txt

Footer menus
------------
:date: 2022-03-14
:category: Goodies
:tags: pelicanconf.py

This theme provides an optional horizontal menu
located at the bottom if each page: the footer menu.
It could be useful for contact and license entries::

  FOOTERMENUITEMS = (
    ('About',    'about.html'),
    ('Contact',  'contact.html'),
    ('Links',    'links.html'),
    ('License',  'license.html'),
  )

A second bottom horizontal menu presents the `Archives`_ and feeds.
This menu is active by default and could be removed with::

  FOOTERMENUFEED = False

Finally, a third bottom horizontal menu could optionally defined
by the ``SOCIAL`` standard variable of the pelican environment.
This variable is not used in the ``pelicanconf.py`` file of the
present documentation. 
As for the ``MENUITEMS`` and ``FOOTERMENUITEMS`` variables,
it consists of a list of tuples (title, url).
Note that this menu points to pages such as ``about.html``,
and there is a corresponding ``about.rst`` source file
that looks like::

  About
  ------
  :date: 2023-02-14
  :status: hidden

  Yes, we have writen this amazing blog!

Observe that such a file is not a blog article:
it has the status *hidden*
and do not appear in the navigation of articles.
