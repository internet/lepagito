.. include:: common.txt

Article navigation
------------------
:date: 2021-05-14
:category: Options
:tags: navigation, plugin, pelicanconf.py

The navigation between two successive pagination of article summaries
is presented at the bottom:
it provides both Oldest/Newest pages and also Fist/Last,
for efficiency.
When reading articles, the navigation between previous and next articles
is presented both at the top and the bottom.
This navigation feature is implemented thanks to the pelican `neighbors`_ plugin.

By default, the oldest pages and article navigation links
appear on the right, and newest on the left.
The ``LEPAGITO_NAVIGATION_SWAP`` variable in the ``pelicanconf.py`` file
allows to swap from left to write this navigation.
This feature is useful when the chronology of a blog is not important,
as in the present documentation of the `lepagito`_ theme:
the newest article means only the first article to read.
