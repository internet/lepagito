# -----------------------------------
# check for standard pelican settings
# -----------------------------------
# THEME shoud always be defined: no default value
#   see https://docs.getpelican.com/en/latest/settings.html
import sys
#sys.stderr.write (f'themeconf.py: started...\n')
try:    THEME
except: raise SystemExit (f'lepagito/themeconf.py: THEME variable not defined')
try:    OUTPUT_PATH
except: OUTPUT_PATH = 'output/' # default value
try:    PLUGINS
except: PLUGINS = None # default value
try:    PLUGIN_PATHS
except: PLUGIN_PATHS = [] # default value

# -----------------------------------
# lepagito config
# -----------------------------------
import os
if os.path.basename (THEME.removesuffix ('/')) == 'lepagito':
    try:    LEPAGITO_COLOR
    except: LEPAGITO_COLOR = None
    #sys.stderr.write (f'themeconf.py: THEME = {THEME}\n')
    #sys.stderr.write (f'themeconf.py: LEPAGITO_COLOR = {LEPAGITO_COLOR}\n')
    # {THEME}/lib/lepagitoconf.py: will automatically generate 'theme/css/color.css'
    sys.path.insert (0, f'{THEME}/lib')
    from lepagitoconf import lepagitoconf
    lepagitoconf (LEPAGITO_COLOR, OUTPUT_PATH)
