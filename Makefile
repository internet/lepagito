SUBDIRS = 		\
	doc		\
	templates	\
	static		\
	lib		\

DISTFILES = 		\
	Makefile 	\
	README.rst	\
	themeconf.py	\

all:
	cd doc && $(MAKE) all

install:
	cd doc && $(MAKE) install

clean:
	cd doc && $(MAKE) clean

-include $(CVSMKROOT)/git.mk
