.. _lepagito: http://lepagito.free.fr
.. _Pirogh: http://pirogh.free.fr
.. _Greuze-Cottave: http://greuzecottave.free.fr
.. _galleria: http://galleria.free.fr
.. _Isabelle Lepage: https://www.lafilacroche.com

`Lepagito`_ is a lovely pelican theme.

Observe it first in action `lepagito`_ web site.
Also, on the `Pirogh`_  author's blog,
on the `Greuze-Cottave`_ painters blog
and on the `galleria`_ pelican plugin documentation.


You're interested? Fascinated? Let's start!
See the `lepagito`_ documentation web site for installation and usage.
